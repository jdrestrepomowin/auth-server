const { response } = require('express');//para tner ayudas de autocompletado en nuestros controladores
const { validationResult } = require('express-validator');


const createUser = ( req, res = response) => {
    
    

    const { email, name, password } = req.body;
    
    return res.json({
        ok: true,
        msg: 'Crear usuario /new'
    })
}

const userLogin = ( req, res = response) => {

    const { email, password } = req.body;
    
    return res.json({
        ok: true,
        msg: 'Login de usuario /'
    })
}

const renewToken = ( req, res = response) => {
    return res.json({
        ok: true,
        msg: 'renew /renew'
    })
}

module.exports = {
    createUser,
    userLogin,
    renewToken
}