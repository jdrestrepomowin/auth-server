const { Router } = require('express');
const { check } = require('express-validator');
const { createUser,userLogin, renewToken } = require('../controllers/auth.controller');
const { validateInputs } = require('../middlewares/validar-campos');

const router = Router();

//Crear un nuevo usuario
router.post( '/new' , [
    check('name', 'nombre es Obligatorio y debe tener mas de 6 caracteres').isLength({min: 6}),
    check('email', 'El email no es valido').isEmail(),
    check('password', 'El password es obligatorio').isLength({min: 6}),
    validateInputs
 ] , createUser );

//Login de usuario
router.post( '/' , [
    check('email', 'El email no es valido').isEmail(),
    check('password', 'El password es obligatorio').isLength({min: 6}),
    validateInputs
], userLogin);

//Validar y revalidar token
router.get( '/' , renewToken );

module.exports = router;